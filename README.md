# AWS S3 Bucket Deployment with CDK and CodeWhisperer

This project demonstrates the generation and deployment of an Amazon S3 bucket using the AWS Cloud Development Kit (CDK), with Amazon CodeWhisperer helping guide the coding. It showcases how to leverage CodeWhisperer for rapid development and deployment of cloud resources on AWS.

## Overview

The AWS CDK provides a high-level framework to define cloud infrastructure in code and provision it through AWS CloudFormation. Combined with the AI-driven code generation capabilities of Amazon CodeWhisperer, this project streamlines the development process of creating and configuring AWS resources.

## Setup

To run this project, ensure you have the following prerequisites installed:

- AWS CLI
- Node.js and npm
- AWS CDK Toolkit

Then, clone the repository to your local machine:

```bash
git clone https://gitlab.com/hm222/s3-bucket.git
cd s3-bucket
```

## S3 Bucket Features

The deployed S3 bucket is configured with **versioning** and **encryption** to enhance data protection and retrievability:

- **Versioning**: Enabled to keep multiple variants of an object in the same bucket. This feature is crucial for data recovery and protection against accidental overwrites and deletions.

- **Encryption**: The bucket uses S3-managed encryption keys to automatically encrypt data. This ensures that your data is securely stored within the bucket.

## Deployment

The S3 bucket is deployed by running:

```bash
cdk deploy
```

with the appropriate credentials and default region configured on the laptop.

## Usage

The S3 bucket created by this project can be used for various applications, such as hosting static web content, storing application data, or archiving.

## Screenshots

### CodeWhisperer Usage

![CodeWhisperer Usage](codewhisperer-usage.png)

This screenshot demonstrates how Amazon CodeWhisperer was used to help generate the temmplate code for deploying an S3 bucket.

### Generated S3 Bucket on AWS

![Generated S3 Bucket](generated-s3-bucket.png)

This screenshot displays the S3 bucket successfully created and deployed on AWS.